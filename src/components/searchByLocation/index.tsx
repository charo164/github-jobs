import React, { useContext } from "react";
import { Data } from "../../App";
import "./searchByLocation.scss";
import FullTime from "../fulltime/index";
export default function SearchByLocation() {
  const { fullTimeCheckBox, setFullTime, setLocation } = useContext(Data);

  const sLocation = (e) => {
    if (e.target) {
      if (e.target.type === "radio") {
        const searchByLoc: any = document.querySelector(
          "input[name=searchByLoc]"
        );
        if (searchByLoc) {
          searchByLoc.value = "";
        }
        setLocation(e.target.value);
      } else {
        const radio: any = document.querySelector(
          "input[name=country]:checked"
        );
        if (radio) {
          radio.checked = false;
        }
        if (e.target.value.length > 2) {
          setLocation(e.target.value);
        } else if (e.target.value === "") {
          setLocation(e.target.value);
        }
      }
    }
  };
  return (
    <section className="searchByLocation">
      <FullTime setFullTime={setFullTime} fullTimeCheckBox={fullTimeCheckBox} />
      <div className="location">
        <h2>location</h2>
        <input
          type="text"
          placeholder="City, state or country"
          name="searchByLoc"
          onInput={sLocation}
        />
        <form>
          <input
            type="radio"
            id="London"
            name="country"
            value="London"
            onChange={sLocation}
          />
          <label htmlFor="London">London</label>
          <br />
          <input
            type="radio"
            id="Amsterdam"
            name="country"
            value="Amsterdam"
            onChange={sLocation}
          />
          <label htmlFor="Amsterdam">Amsterdam</label>
          <br />
          <input
            type="radio"
            id="New York"
            name="country"
            value="New York"
            onChange={sLocation}
          />
          <label htmlFor="New York">New York</label>
          <br />
          <input
            type="radio"
            id="Berlin"
            name="country"
            value="Berlin"
            onChange={sLocation}
          />
          <label htmlFor="Berlin">Berlin</label>
        </form>
      </div>
    </section>
  );
}
