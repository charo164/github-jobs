import React, { useContext, useState, useEffect } from "react";
import FakeJobs from "../fakeJobs/index";
import JobsListGenerator from "../jobsListGenerator";
import Pagination from "rc-pagination";
import { Data } from "../../App";
import "./jobs.scss";
import "./pagination.css";
import empty from "../../img/scene_fp_empty_state.png";

export default function Jobs() {
  const {
    jobs,
    truncate,
    timeCalculator,
    startIndex,
    setStartIndex,
    currentPage,
    setCurrentPage,
    searchBar,
    description,
  } = useContext(Data);
  const [maxItems] = useState(5);

  const changeCurrentPage = (numPage) => {
    setCurrentPage(numPage);
    setStartIndex(indexCalculator(numPage));
  };

  const indexCalculator = (n) => {
    const a = n * maxItems;
    const b = a - 1;
    const c = b - 4;
    return c;
  };

  let results;

  if (jobs[0] === "default") {
    results = (
      <section className="jobs">
        <FakeJobs />
        <FakeJobs />
        <FakeJobs />
        <FakeJobs />
        <FakeJobs />
        <FakeJobs />
      </section>
    );
  } else if (jobs[0] === "noResult") {
    results = (
      <section className="jobs">
        <>
          <img src={empty} alt="empty-img" className="empty" />
          <h2 className="noResult">No results !</h2>
        </>
      </section>
    );
  } else if (jobs[0] === "error") {
    results = (
      <section className="jobs">
        <>
          <img src={empty} alt="empty-img" className="empty" />
          <h2 className="noResult">Error ! try refresh the page</h2>
        </>
      </section>
    );
  } else {
    results = (
      <section className="jobs">
        <JobsListGenerator
          jobs={jobs}
          startIndex={startIndex}
          maxItems={maxItems}
          truncate={truncate}
          timeCalculator={timeCalculator}
        />
        <div className="PaginationContainer">
          <Pagination
            total={jobs.length}
            onChange={changeCurrentPage}
            current={currentPage}
            pageSize={maxItems}
          />
        </div>
      </section>
    );
  }
  useEffect(() => {
    if (searchBar.current) {
      searchBar.current.value = description;
    }
    // eslint-disable-next-line
  }, []);
  return results;
}
