import React from "react";

export default function FullTime({ fullTimeCheckBox, setFullTime }) {
  const checkFullTime = () => {
    if (fullTimeCheckBox.current) {
      if (fullTimeCheckBox.current.checked) {
        setFullTime(true);
      } else {
        setFullTime(false);
      }
    }
  };

  return (
    <div className="fulltime">
      <input
        type="checkbox"
        name="fulltime"
        ref={fullTimeCheckBox}
        onChange={checkFullTime}
      />
      <label htmlFor="fulltime">Full time</label>
    </div>
  );
}
