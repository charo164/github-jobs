import React from "react";
import gth from "../../img/github.png";
import "./header.scss";
export default function Header() {
  return (
    <header className="header">
      <h1>
        <b>Github</b> Jobs
      </h1>
      <a
        href="https://github.com/charo164"
        target="_blank"
        rel="noopener noreferrer"
      >
        <img src={gth} alt="github-logo" />
      </a>
    </header>
  );
}
