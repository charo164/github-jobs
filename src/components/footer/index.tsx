import React from "react";
import "./footer.scss";

export default function Footer() {
  return (
    <footer className="footer">
      <h2 className="author">
        <a
          href="https://github.com/charo164"
          target="_blank"
          rel="noopener noreferrer"
        >
          {" "}
          Jules Jacques Coly
        </a>{" "}
        @{" "}
        <a
          href="http://DevChallenges.io"
          target="_blank"
          rel="noopener noreferrer"
        >
          DevChallenges.io{" "}
        </a>
      </h2>
    </footer>
  );
}
