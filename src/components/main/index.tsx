import React from "react";
import Search from "../search";
import Jobs from "../jobs";
import SearchByLocation from "../searchByLocation";
import "./main.scss";

export default function Main() {
  return (
    <main className="main">
      <Search />
      <div className="jobList">
        <SearchByLocation />
        <Jobs />
      </div>
    </main>
  );
}
