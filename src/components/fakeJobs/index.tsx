import React from "react";

export default function FakeJobs() {
  return (
    <div className="job fakeJ">
      <div className="CompLogo"></div>
      <div className="jobInfo jobInfoFake">
        <div className="preview">
          <h3 className="compName">fake</h3>
          <h2 className="titl">fake</h2>
          <h4 className="full">fake</h4>
        </div>
        <div className="moreInfo">
          <span className="locationInfo">
            <b className="fakeInfo"></b>
          </span>
          <span className="dateInfo">
            <b className="fakeInfo"></b>
          </span>
        </div>
      </div>
    </div>
  );
}
