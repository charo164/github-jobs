import React from "react";
import { Link } from "react-router-dom";
import "./page404.scss";
export default function Page404() {
  return (
    <main className="page404">
      <h1>page not found</h1>
      <h2>404</h2>
      <Link to="/">Go back</Link>
    </main>
  );
}
