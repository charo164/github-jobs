import React from "react";
import { Link } from "react-router-dom";
export default function JobsListGenerator({
  jobs,
  startIndex,
  maxItems,
  truncate,
  timeCalculator,
}) {
  return jobs.map((e, i) => {
    if (i >= startIndex && i <= startIndex + maxItems - 1) {
      return (
        <div className="job list" key={i}>
          <div className="CompLogo">
            {e.company_logo ? (
              <img src={e.company_logo} alt="company_logo" />
            ) : (
              <b>not found</b>
            )}
          </div>
          <div className="jobInfo">
            <div className="preview">
              <h3> {e.company} </h3>
              <h2>
                <Link to={`/detail/${i}`}>{e.title}</Link>
              </h2>
              <h4> {e.type} </h4>
            </div>
            <div className="moreInfo">
              <span className="locationInfo">
                🌍
                {e.location.length > 12 ? truncate(e.location, 13) : e.location}
              </span>
              <span className="dateInfo">
                🕑 {timeCalculator(e.created_at)}
              </span>
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  });
}
