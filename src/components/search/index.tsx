import React, { useContext } from "react";
import { Data } from "../../App";
import "./search.scss";

export default function Search() {
  const { setDescription, searchBar } = useContext(Data);
  const handleData = (e) => {
    if (e.target.value.length > 2) {
      setDescription(e.target.value);
    } else if (e.target.value === "") {
      setDescription(e.target.value);
    }
  };
  return (
    <section className="search">
      <form onSubmit={(e) => e.preventDefault()}>
        <span className="searchBar">
          <input
            type="text"
            placeholder="💼 Title, companies, expertise or benefits"
            onChange={handleData}
            required
            ref={searchBar}
          />
          <button type="submit">Search</button>
        </span>
      </form>
    </section>
  );
}
