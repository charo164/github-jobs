import React, { useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import "./detail.scss";
import parse from "html-react-parser";
export default function Detail({ jobs, timeCalculator, truncate }) {
  const { id } = useParams();

  const job = jobs[id];

  const changeLinkContent = async () => {
    window.scrollTo(0, 0);
    const link1 = await document.querySelectorAll(".howToApply a");
    const link2 = await document.querySelectorAll(".description a");
    await link1.forEach((e) => {
      e.innerHTML = `click here`;
    });
    await link2.forEach((e) => {
      e.innerHTML = `click here`;
    });
  };

  changeLinkContent();

  useEffect(() => {
    changeLinkContent();
  }, []);

  return typeof job === "object" ? (
    <main className="detail">
      <section className="left">
        <Link to="/" className="back">
          <b>{"<"}</b> Back to search
        </Link>
        <h2>How to Apply</h2>
        <div className="howToApply">{parse(job.how_to_apply)}</div>
      </section>
      <section className="right">
        <h2>
          {job.title} <b> {job.type} </b>
        </h2>
        <p className="date">🕑 {timeCalculator(job.created_at)}</p>
        <div className="lil">
          <div className="log">
            {job.company_logo ? (
              <img src={job.company_logo} alt="company_logo" />
            ) : (
              <b>not found</b>
            )}
          </div>
          <div className="locCn">
            <h3> {job.company} </h3>
            <span>
              🌍
              {job.location.length > 12
                ? truncate(job.location, 13)
                : job.location}
            </span>
          </div>
        </div>
        <div className="description">{parse(job.description)}</div>
      </section>
    </main>
  ) : (
    <main className="detail">
      <Link to="/">{"<"} Back to search</Link>
    </main>
  );
}
