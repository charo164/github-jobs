import React, { useEffect, useState, useRef } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import topbar from "topbar";
import Header from "./components/header";
import Footer from "./components/footer";
import Main from "./components/main";
import Page404 from "./components/404Page";
import Detail from "./components/detail";
import "./App.scss";

export const Data = React.createContext<any>(null);

export default function App() {
  const fullTimeCheckBox = useRef<HTMLInputElement>(null);
  const searchBar = useRef<HTMLInputElement>(null);
  const [location, setLocation] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [fullTime, setFullTime] = useState<boolean>(false);
  const [jobs, setJobs] = useState<any[]>(["default"]);
  const [allJobs, setAllJobs] = useState<any[]>(["default"]);
  const [startIndex, setStartIndex] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);

  topbar.config({
    autoRun: true,
    barThickness: 3,
    barColors: {
      0: "#1e86ff",
    },
    shadowBlur: 10,
    shadowColor: "rgba(0,   0,   0,   .6)",
  });

  const truncate = (text, n) => {
    return `${text.slice(0, n)}...`;
  };

  const timeCalculator = (d) => {
    const date = Date.parse(d) / 1000;
    const dateNow = Date.now() / 1000;
    const s = dateNow - date;
    const mn = Math.round(s / 60);
    const hour = Math.round(mn / 60);
    const day = Math.round(hour / 24);
    const month = Math.round(day / 31);
    const year = Math.round(month / 12);
    if (year <= 0) {
      if (month <= 0) {
        if (day <= 0) {
          if (hour <= 0) {
            if (mn <= 0) {
              return ` ${s}s ago`;
            } else {
              return ` ${mn}mn ago`;
            }
          } else {
            return ` ${hour}h ago`;
          }
        } else {
          return `${day}d ago`;
        }
      } else {
        return month <= 1 ? `${month} month ago` : `${month}months ago`;
      }
    } else {
      return year <= 1 ? `${year} year ago` : `${year}years ago`;
    }
  };

  const filter = (location, description, full_time, jbs) => {
    const a = jbs.filter((j) => {
      return (
        j.location
          .trim()
          .toLowerCase()
          .search(location.trim().toLowerCase()) !== -1
      );
    });
    const b = a.filter((j) => {
      if (full_time) {
        return j.type.trim().toLowerCase() === "Full Time".toLowerCase();
      } else {
        return j;
      }
    });
    const c = b.filter((j) => {
      return (
        j.company
          .trim()
          .toLowerCase()
          .search(description.trim().toLowerCase()) !== -1 ||
        j.description
          .trim()
          .toLowerCase()
          .search(description.trim().toLowerCase()) !== -1 ||
        j.title
          .trim()
          .toLowerCase()
          .search(description.trim().toLowerCase()) !== -1 ||
        j.type.trim().toLowerCase().search(description.trim().toLowerCase()) !==
          -1
      );
    });
    topbar.hide();
    return c.length > 0 ? c : ["noResult"];
  };

  const getData = async () => {
    let count = 0;
    const json = (p) => {
      count++;
      return p.json();
    };
    const page1 = await fetch(
      `https://cors-anywhere.herokuapp.com/https://jobs.github.com/positions.json`
    );
    const page2 = await fetch(
      `https://cors-anywhere.herokuapp.com/https://jobs.github.com/positions.json?page=2`
    );
    const page3 = await fetch(
      `https://cors-anywhere.herokuapp.com/https://jobs.github.com/positions.json?page=3`
    );

    const page1Json = await json(page1);
    const page2Json = await json(page2);
    const page3Json = await json(page3);

    if ((await count) === 3) {
      const j: any = [];
      page1Json.map((e) => {
        j.push(e);
        return null;
      });
      page2Json.map((e) => {
        j.push(e);
        return null;
      });
      page3Json.map((e) => {
        j.push(e);
        return null;
      });
      return j;
    }
  };

  useEffect(() => {
    const search = (description, location, full_time) => {
      setStartIndex(0);
      setCurrentPage(1);
      topbar.show();
      if (allJobs[0] === "default") {
        getData()
          .then((jbs) => {
            if (jbs.length > 0) {
              setJobs(jbs);
              setAllJobs(jbs);
              topbar.hide();
            } else {
              setJobs(["noResult"]);
              topbar.hide();
            }
          })
          .catch((e) => {
            topbar.hide();
            setJobs(["error"]);
          });
      } else {
        setJobs(filter(location, description, full_time, allJobs));
      }
    };
    search(description, location, fullTime);
  }, [allJobs, description, fullTime, location]);

  return (
    <BrowserRouter>
      <div className="app">
        <div className="container">
          <Header />
          <Switch>
            <Route
              path="/"
              exact={true}
              children={
                <Data.Provider
                  value={{
                    setFullTime,
                    fullTimeCheckBox,
                    searchBar,
                    setLocation,
                    setDescription,
                    location,
                    description,
                    fullTime,
                    jobs,
                    truncate,
                    timeCalculator,
                    startIndex,
                    setStartIndex,
                    currentPage,
                    setCurrentPage,
                  }}
                >
                  <Main />
                </Data.Provider>
              }
            />
            <Route
              path="/detail/:id"
              exact={true}
              children={
                <Detail
                  jobs={jobs}
                  timeCalculator={timeCalculator}
                  truncate={truncate}
                />
              }
            />
            <Route children={<Page404 />} />
          </Switch>
          <Footer />
        </div>
      </div>
    </BrowserRouter>
  );
}
